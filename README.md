# Aplicação Firebase Angular

Aplicação utilizada no projeto de PI/1 do curso de sistema de informação do centro universitario UNA.


## Integrantes

- Clayton Fellipe
- Carlos Douglas
- Samuel Franklin
- Gleidson Ferreira
- Paulo Henrique
- Jeferson Matheus


## Utilizando a aplicação

### Frontend

O frontend está localizado na pasta *"webapp"* e é inicialização [Angular](https://angular.io/), então para sua utilização é necessário a instalação do [Angular CLI](https://cli.angular.io/) e rodar o comando ``ng serve`` e claro a instalação das dependências do projeto por meio do ``npm install``

Documentação do angular:

#### Estrutura utilizada

- e2e
- node_modules
- src
    - app
        - _components
        - _guards
        - _models
        - _services
        - _utils
        - containers e outros
    - assets
    - environments


### Backend

O backend está localizado na pasta *"api"* e é utilizado o [firebase functions](https://firebase.google.com/docs/functions/) com [node.js](https://nodejs.org/en/). É necessário a instalação do [firebase CLI](https://firebase.google.com/docs/cli/) e rodar o comando ``firebase serve --only funcitons``


#### Estrutura utilizada

- functions
    - node_modules
    - src
        - models
        - controllers
        - services
