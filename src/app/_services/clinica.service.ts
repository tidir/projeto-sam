import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { DatabaseAdpter } from '../_utils/dabase-adpter';
import { PontoAtendimento } from '../_models/ponto-atendimento';
import { Http } from '@angular/http';

@Injectable()
export class ClinicaService {
  readonly STORAGE_KEY = '/clinica/';
  readonly API = `${environment.api}pontos-atendimento/`;
  // cria uma variavel que guarda os dados da API a ser consultada.

  constructor(private db: DatabaseAdpter, private http: Http) {}

  getbyid(id) {
    return this.http.get(this.API + id);
  }

  get() {
    // consulta os dados guardados na variavel API e traz o selecionado.
    return this.http.get(this.API);
  }

  getAll() {
    return this.db.get(this.STORAGE_KEY);
  }

  post(clinica: PontoAtendimento) {
    return this.db.post(clinica, this.STORAGE_KEY);
  }

  put(clinica: PontoAtendimento) {
    return this.db.put(clinica, this.STORAGE_KEY);
  }

  delete(clinica: PontoAtendimento) {
    this.db.delete(clinica.id, this.STORAGE_KEY);
  }
}
