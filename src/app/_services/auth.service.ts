import { Injectable, Output, EventEmitter } from '@angular/core';
import { User } from '../_models/usuario';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { ToastrService } from 'ngx-toastr';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import { Papel } from '../_models/papel';
import { PontoAtendimento } from '../_models/ponto-atendimento';

@Injectable()
export class AuthService {
  readonly STORAGE_KEY = '/usuarios/';
  readonly LOCALSTORAGE_KEY = 'logged';
  readonly LOCALSTORAGE_CURRENT = 'current';

  @Output() depoisDeLogar = new EventEmitter();
  @Output() erroLogin = new EventEmitter();
  @Output() depoisDeCriarConta = new EventEmitter();
  @Output() depoisDeDeslogar = new EventEmitter();
  @Output() depoisDeResetar = new EventEmitter();

  usuario = {};

  constructor(
    private afAuth: AngularFireAuth,
    private afDB: AngularFireDatabase,
    private toastrService: ToastrService
  ) {
    this.afAuth.auth.onAuthStateChanged(this._recuperarInfoUsuario.bind(this));

    this.afAuth.authState.subscribe(resp => {
      if (resp) {
        this._recuperarInfoUsuario(resp);
      }
    });
  }

  entrar(email: string, senha: string) {
    this.afAuth.auth
      .signInWithEmailAndPassword(email, senha)
      .then(usuario => {
        if (usuario) {
          localStorage.setItem(this.LOCALSTORAGE_KEY, 'true');
          this.afDB.object(`/usuarios/${usuario.uid}`)
            .valueChanges()
            .subscribe(snap => {
              this.usuario = snap;
              localStorage.setItem(this.LOCALSTORAGE_CURRENT, JSON.stringify(snap));
              this.depoisDeLogar.emit(usuario);
            });
        }
      })
      .catch(err => {
        if (err) {
          this.toastrService.error(err);
          this.erroLogin.emit();
        }
      });
  }

  entrarComGoogle() {
    this.afAuth.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(usuario => {
        this._recuperarInfoUsuario(usuario);
        localStorage.setItem(this.LOCALSTORAGE_KEY, 'true');
        this.depoisDeLogar.emit();
      });
  }

  criarConta(email: string, senha: string, usuario: User, clinica: PontoAtendimento | any | boolean = false) {
    this.afAuth.auth
      .createUserWithEmailAndPassword(email, senha)
      .then(user => {
        if (user) {
          if (clinica) {
            const updatesClinica = {};
            usuario.papel = new Papel('Administrador', 2);
            clinica.dono = user.uid;
            updatesClinica['pontos-atendimento/' + this.afDB.createPushId()] = clinica;
            this.afDB.app
              .database()
              .ref()
              .update(updatesClinica);
          }

          const updates = {},
            ref = this.STORAGE_KEY + user.uid;
          updates[ref] = usuario;
          delete updates[ref]['senha'];
          this.afDB.app
            .database()
            .ref()
            .update(updates);

          user
            .updateProfile({
              displayName: `${usuario.nome}`
            })
            .then(() => {
              this._recuperarInfoUsuario(user);
              this.depoisDeCriarConta.emit();
            });

          localStorage.setItem(this.LOCALSTORAGE_KEY, 'true');
        }
      })
      .catch(err => {
        if (err) {
          this.toastrService.error(err);
        }
      });
  }

  resetarSenha(email: string) {
    this.afAuth.auth
      .sendPasswordResetEmail(email)
      .then(success => {
        this.depoisDeResetar.emit();
        this.toastrService.success(
          'Solicitação enviada com sucesso, enviamos um passo a passo para seu e-mail'
        );
      })
      .catch(err => {
        if (err) {
          this.toastrService.error(err);
        }
      });
  }

  isLogado(): boolean {
    if (localStorage.getItem(this.LOCALSTORAGE_KEY)) {
      return true;
    }
    return false;
  }

  sair() {
    this.afAuth.auth.signOut()
    .then(info => {
      localStorage.setItem(this.LOCALSTORAGE_KEY, 'false');
      localStorage.removeItem(this.LOCALSTORAGE_KEY);
      localStorage.removeItem(this.LOCALSTORAGE_CURRENT);
      this.usuario = {};
      localStorage.clear();
      this.toastrService.success('Você saiu', 'Usuário deslogado com sucesso');
      this.depoisDeDeslogar.emit();
    }).catch( err => {
      this.toastrService.error('Falha ao deslogar', 'Ocorreu algum erro ao tentar deslogar, tente novamente');
    });
  }

  private _recuperarInfoUsuario(user) {
    const atual = localStorage.getItem(this.LOCALSTORAGE_CURRENT);
    if (atual) {
      this.usuario = JSON.parse(atual);
      this.depoisDeLogar.emit();
    }
  }
}
