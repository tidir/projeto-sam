import { Injectable } from '@angular/core';
import { DatabaseAdpter } from '../_utils/dabase-adpter';
import { Exemplo } from '../_models/exemplo';

@Injectable()
export class UsuarioService {
  readonly STORAGE_KEY = '/usuario/';

  constructor(private db: DatabaseAdpter) {}

  getAll() {
    return this.db.get(this.STORAGE_KEY);
  }

  getById(id) {
    return this.db.getById(id, this.STORAGE_KEY);
  }

  post(exemplo: Exemplo) {
    return this.db.post(exemplo, this.STORAGE_KEY);
  }

  put(exemplo: Exemplo) {
    return this.db.put(exemplo, this.STORAGE_KEY);
  }

  delete(exemplo: Exemplo) {
    this.db.delete(exemplo.id, this.STORAGE_KEY);
  }
}
