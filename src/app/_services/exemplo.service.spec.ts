import { TestBed, inject } from '@angular/core/testing';

import { ExemploService } from './exemplo.service';

describe('ExemploService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExemploService]
    });
  });

  it('should be created', inject([ExemploService], (service: ExemploService) => {
    expect(service).toBeTruthy();
  }));
});
