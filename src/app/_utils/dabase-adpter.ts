import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from '../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { throws } from 'assert';
import { AbastractFirebaseModel } from '../_models/abstract-firebase-model';

@Injectable()
export class DatabaseAdpter {

  constructor(private _afDB: AngularFireDatabase) { }

  async query(path, orderBy, equals) {
    return this._afDB.list(path,  ref => ref.orderByChild(orderBy).equalTo(equals)).valueChanges();
  }

   getById(id: string, path: string) {
    return this._afDB.object(`${path}/${id}`).valueChanges();
  }

   get(path: string) {
    return this._afDB.list(path).valueChanges();
  }

   post(entity: AbastractFirebaseModel, path: string) {
    entity.id = this._afDB.createPushId();
    return this._afDB.object(`${path}/${entity.id}`).set(entity);
  }

   put(entity: AbastractFirebaseModel, path: string) {
    entity.__v++;
    entity.atualizadoEm =  Date.now().toString();
    return this._afDB.object(`${path}/${entity.id}`).update(entity);
  }

   delete(id: string, path: string) {
    return this._afDB.object(`${path}/${id}`).remove();
  }
}
