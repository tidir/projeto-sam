import { Subscriber } from 'rxjs/Subscriber';

export class UnsubscribeAll {

  private _list = Array<Subscriber<any>>();

  public add(subscribe: Subscriber<any> | any) {
    this._list.push(subscribe);
  }

  public exec() {
    this._list.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
