import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { UnsubscribeAll } from '../_utils/unsubscribe-all';
import { ClinicaService } from '../_services/clinica.service';
import { PontoAtendimento } from '../_models/ponto-atendimento';

@Component({
  selector: 'app-clinica-page',
  templateUrl: './clinica-page.component.html',
  styleUrls: ['./clinica-page.component.css']
})
export class ClinicaPageComponent implements OnInit, OnDestroy {

  public logado = false;
  public clinica = new PontoAtendimento;
  public clinicas;
  private _unsubscribe = new UnsubscribeAll();
  public siglas = ['MG', 'SP', 'RJ'];


  constructor(private authService: AuthService, private clinicaService: ClinicaService) { }

  ngOnInit() {
    this.logado = this.authService.isLogado();

    this._unsubscribe.add(
      this.authService.depoisDeLogar
      .subscribe(usuario => {
        this.logado = true;
      })
    );

    this._unsubscribe.add(
      this.authService.depoisDeDeslogar
        .subscribe(info => {
          this.logado = false;
        })
    );
    this.clinicas = this.clinicaService.getAll();
  }

  salvar() {
    if (this.clinica.id) {
      this.clinicaService.put(this.clinica);
    } else {
      this.clinicaService.post(this.clinica);
    }
    this.clinica = new PontoAtendimento;
  }

  selecionar(clinica: PontoAtendimento) {
    this.clinica = clinica;
  }

  excluir(clinica: PontoAtendimento) {
    this.clinicaService.delete(clinica);
  }

  ngOnDestroy() {
    this._unsubscribe.exec();
  }

}
