import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicaPageComponent } from './clinica-page.component';

describe('ClinicaPageComponent', () => {
  let component: ClinicaPageComponent;
  let fixture: ComponentFixture<ClinicaPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicaPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
