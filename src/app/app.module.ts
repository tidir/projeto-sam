import { UsuarioService } from './_services/usuario.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { RodapeComponent } from './_components/rodape/rodape.component';
import { AppRoutingModule } from './app-routing.module';
import { HomePageComponent } from './home-page/home-page.component';
import { CabecarioComponent } from './_components/cabecario/cabecario.component';
import { ExemploPageComponent } from './exemplo-page/exemplo-page.component';
import { AuthService } from './_services/auth.service';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { ToastrModule } from 'ngx-toastr';
import { ExemploService } from './_services/exemplo.service';
import { ClinicaService } from './_services/clinica.service';
import { DatabaseAdpter } from './_utils/dabase-adpter';
import { ClinicaPageComponent } from './clinica-page/clinica-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthGuard } from './_guard/auth.guard';
import { EsqueciASenhaComponent } from './esqueci-a-senha/esqueci-a-senha.component';
import { PacienteService } from './_services/paciente.service';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';
import { MarcacaoPageComponent } from './marcacao-page/marcacao-page.component';
import { HttpModule } from '@angular/http';
import { PontoAtendimentoPageComponent } from './ponto-atendimento-page/ponto-atendimento-page.component';

@NgModule({
  declarations: [
    AppComponent,
    RodapeComponent,
    HomePageComponent,
    CabecarioComponent,
    ExemploPageComponent,
    ClinicaPageComponent,
    LoginPageComponent,
    EsqueciASenhaComponent,
    CadastroUsuarioComponent,
    MarcacaoPageComponent,
    PontoAtendimentoPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true
    }),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    NgbModule.forRoot(),
    HttpModule
  ],
  providers: [
    DatabaseAdpter,
    AuthService,
    ExemploService,
    ClinicaService,
    AuthGuard,
    PacienteService,
    UsuarioService,
    { provide: LOCALE_ID, useValue: 'pt'  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
