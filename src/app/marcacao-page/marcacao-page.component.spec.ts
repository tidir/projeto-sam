import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcacaoPageComponent } from './marcacao-page.component';

describe('MarcacaoPageComponent', () => {
  let component: MarcacaoPageComponent;
  let fixture: ComponentFixture<MarcacaoPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcacaoPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcacaoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
