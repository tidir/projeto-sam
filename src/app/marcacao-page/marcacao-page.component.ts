import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-marcacao-page',
  templateUrl: './marcacao-page.component.html',
  styleUrls: ['./marcacao-page.component.css']
})
export class MarcacaoPageComponent implements OnInit {
  hoje: NgbDateStruct;

  agendamento: any = {};

  constructor() { }

  ngOnInit() {
    const agora = new Date();
    this.hoje = {
      day: agora.getDate(),
      month: agora.getMonth() + 1,
      year: agora.getFullYear()
    };
  }

  marcarConsulta() {
    alert('marcar consulta');
  }

}
