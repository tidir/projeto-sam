export abstract class AbastractFirebaseModel {
  public id: string;
  private _criadoEm: string;
  private _atualizadoEm: string;
  private _criadoPor: string;
  __v: number;

  constructor(criadoEm?: string) {
    this.__v = 0;
    this._criadoEm = Date.now().toString();
    if (criadoEm) {
      this._criadoEm = criadoEm;
    }
  }

  get criadoEm(): Date {
    return new Date(this._criadoEm);
  }

  get atualizadoEm(): any {
    return new Date(this._criadoEm);
  }

  set atualizadoEm(data) {
    this._atualizadoEm = data;
  }

  equals(object: AbastractFirebaseModel): boolean {
    return this.id === object.id;
  }

  toString(): string {
    let str = '{\n';
    Object.keys(this)
      .map(key => {
        str += `\t"${key}":`;
        if (typeof this[key] === 'string') {
          str += `"${this[key]}",\n `;
        }  else if ( typeof this[key] === 'object') {
          str += `: ${this[key].toString()},\n`;
        } else {
          str += `: ${this[key]},\n`;
        }
      });

    str = str.slice(0, str.length - 5);
    str += '\n}';

    return str;
  }

}
