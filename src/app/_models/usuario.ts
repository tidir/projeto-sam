import { AbastractFirebaseModel } from './abstract-firebase-model';
import { Papel } from './papel';

export class User extends AbastractFirebaseModel {
  uid: string;
  nome: string;
  email: string;
  senha: string;
  img_url: string;
  papel: Papel;
  ativo: boolean;
  dataNascimento: string;
  cpf: string;
  tel: string;
  sexo: string;

  constructor() {
    super();
    this.papel = new Papel('user', 0);
  }
}
