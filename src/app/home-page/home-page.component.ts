import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ClinicaService } from './../_services/clinica.service';
import { UnsubscribeAll } from './../_utils/unsubscribe-all';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit, OnDestroy {
  fundo = 'bg-1';
  contador = 0;
  interval;
  pesquisa = { q: '', plano: 'todos' };

  pontoAtendimento: Array<any>;
  _unsubscribe = new UnsubscribeAll();

  constructor(private router: Router, private paService: ClinicaService  ) {}

  ngOnInit() {
    // recebe dados da API:
    this._unsubscribe.add(
      this.paService
        .get()
        .map(data => data.json())
        .subscribe(dados => {
          this.pontoAtendimento = dados.lista;
        })
    );

    this.contador = 0;
    this.interval = setInterval(() => {
      if (this.contador === 2000) {
        if (this.fundo !== 'bg-2') {
          this.fundo = 'bg-2';
        } else {
          this.fundo = 'bg-1';
        }
        this.contador = 0;
      }

      this.contador++;
    }, 1);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  buscar() {
    this.router.navigate(['busca'], {
      queryParams: this.pesquisa
    });
  }
}
