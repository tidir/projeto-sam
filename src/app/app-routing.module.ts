import { PontoAtendimentoPageComponent } from './ponto-atendimento-page/ponto-atendimento-page.component';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { ExemploPageComponent } from './exemplo-page/exemplo-page.component';
import { ClinicaPageComponent } from './clinica-page/clinica-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthGuard } from './_guard/auth.guard';
import { EsqueciASenhaComponent } from './esqueci-a-senha/esqueci-a-senha.component';
import { MarcacaoPageComponent } from './marcacao-page/marcacao-page.component';

const routes: Routes = [
  { path: 'login', component: LoginPageComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomePageComponent },
  {
    path: 'exemplo',
    component: ExemploPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'clinica',
    component: ClinicaPageComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'ponto-atendimento/:id',
    component: PontoAtendimentoPageComponent
  }, {
    path: 'ponto-atendimento/:id/agendar-atendimento',
    component: MarcacaoPageComponent
  },
  { path: 'esqueci-a-senha', component: EsqueciASenhaComponent },
  { path: 'cadastro-usuario', component: CadastroUsuarioComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
