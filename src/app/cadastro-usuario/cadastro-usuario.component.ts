import { Component, OnInit } from '@angular/core';
import { User } from '../_models/usuario';
import { AuthService } from '../_services/auth.service';
import { PontoAtendimento } from '../_models/ponto-atendimento';

@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.css']
})
export class CadastroUsuarioComponent implements OnInit {
  mostrarSegundoForm = false;
  usuario = new User();
  pontoAtendimento = new PontoAtendimento();

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.authService.depoisDeCriarConta.subscribe(() => {
      alert('usuario criado com sucesso!');
    });
  }

  changeForm(hasSegundo) {
    this.mostrarSegundoForm = hasSegundo;
  }

  cadastrar() {
    const senha = this.usuario.senha;
    const email = this.usuario.email;
    if(this.mostrarSegundoForm) {
      this.authService.criarConta(email, senha, this.usuario, this.pontoAtendimento);
    } else {
      this.authService.criarConta(email, senha, this.usuario);
    }
  }
}
