import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { UnsubscribeAll } from '../../_utils/unsubscribe-all';

@Component({
  selector: 'app-cabecario',
  templateUrl: './cabecario.component.html',
  styleUrls: ['./cabecario.component.css']
})
export class CabecarioComponent implements OnInit, OnDestroy {

  usuario = {};
  logado = false;
  private _unsubscribe = new UnsubscribeAll();
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.logado = this.authService.isLogado();
    this.usuario = this.authService.usuario;
    this._unsubscribe.add(
      this.authService.depoisDeLogar
      .subscribe(usuario => {
        this.logado = true;
        this.usuario = this.authService.usuario;
      })
    );

    this._unsubscribe.add(
      this.authService.depoisDeDeslogar
        .subscribe(info => {
          this.logado = false;
        })
    );
  }

  ngOnDestroy() {
    this._unsubscribe.exec();
  }

  entrar() {
    // this.authService.entrarComGoogle();
  }

  deslogar() {
    this.authService.sair();
  }
}
