import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { UnsubscribeAll } from '../_utils/unsubscribe-all';
import { Exemplo } from '../_models/exemplo';
import { ExemploService } from '../_services/exemplo.service';

@Component({
  selector: 'app-exemplo-page',
  templateUrl: './exemplo-page.component.html',
  styleUrls: ['./exemplo-page.component.css']
})
export class ExemploPageComponent implements OnInit, OnDestroy {

  public exemplo = new Exemplo;
  public exemplos;
  public mostrarPagina = false;
  private _unsubscribe = new UnsubscribeAll();

  constructor(private authService: AuthService,
              private exemploService: ExemploService) { }

  ngOnInit() {
    this.mostrarPagina = this.authService.isLogado();

    this._unsubscribe.add(
      this.authService.depoisDeLogar
      .subscribe(usuario => {
        this.mostrarPagina = true;
      })
    );

    this._unsubscribe.add(
      this.authService.depoisDeDeslogar
        .subscribe(info => {
          this.mostrarPagina = false;
        })
    );
    this.exemplos = this.exemploService.getAll();
  }

  selecionar(exemplo: Exemplo) {
    this.exemplo = exemplo;
  }

  excluir(exemplo: Exemplo) {
    this.exemploService.delete(exemplo);
  }

  salvar() {
    if (this.exemplo.id) {
      this.exemploService.put(this.exemplo);
    } else {
      this.exemploService.post(this.exemplo);
    }
    this.exemplo = new Exemplo;
  }

  ngOnDestroy() {
    this._unsubscribe.exec();
  }

}
