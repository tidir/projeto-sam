import { PacienteService } from './../_services/paciente.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UsuarioService } from '../_services/usuario.service';
import { UnsubscribeAll } from '../_utils/unsubscribe-all';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit, OnDestroy {
  user: any = {};
  mensagem = '';
  private _unsubscribe = new UnsubscribeAll();

  constructor(
    private authService: AuthService,
    private router: Router,
    private pacienteService: PacienteService,
    private usuarioService: UsuarioService
  ) {
    this._unsubscribe.add(
      this.authService.depoisDeLogar.subscribe(data => {

        const value = this.usuarioService.getById(data.uid)
          .subscribe((usuarioData) => {
            if (usuarioData) {
              let user: any;
              user = usuarioData;
              console.log(user.papel);
              // se ele for um P.A.
              if (user.papel === 2) {
              } else {
              }
              this.router.navigate(['/home']);
            }
          });

          this._unsubscribe.add(value);
      })
    );

    this.authService.erroLogin.subscribe(data => {
      this.mensagem = 'Usuário ou senha incorreto(s)';
    });
  }
  ngOnDestroy() {
    this._unsubscribe.exec();
  }

  ngOnInit() {
    if (this.authService.isLogado) {
      this.authService.sair();
    }
  }

  logar() {
    this.authService.entrar(this.user.email, this.user.password);
    this.mensagem = 'Aguarde enquanto o login é efetuado';
  }
  entrar() {
    this.authService.entrarComGoogle();
  }
}
