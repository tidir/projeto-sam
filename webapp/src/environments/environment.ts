// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  api: 'https://us-central1-pi-2018.cloudfunctions.net/api',
  firebase: {
    apiKey: 'AIzaSyDdWzhvle7NVWsRoIo8OZRipGeGZ4AFuBE',
    authDomain: 'pi-2018.firebaseapp.com',
    databaseURL: 'https://pi-2018.firebaseio.com',
    projectId: 'pi-2018',
    storageBucket: 'pi-2018.appspot.com',
    messagingSenderId: '359476027854'
  }
};
