import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPontoAtendimentoComponent } from './dashboard-ponto-atendimento.component';

describe('DashboardPontoAtendimentoComponent', () => {
  let component: DashboardPontoAtendimentoComponent;
  let fixture: ComponentFixture<DashboardPontoAtendimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardPontoAtendimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPontoAtendimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
