import { ConfiguracoesPageComponent } from './configuracoes-page/configuracoes-page.component';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { ExemploPageComponent } from './exemplo-page/exemplo-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthGuard } from './_guard/auth.guard';
import { EsqueciASenhaComponent } from './esqueci-a-senha/esqueci-a-senha.component';
import { MarcacaoPageComponent } from './marcacao-page/marcacao-page.component';
import { PontoAtendimentoPageComponent } from './ponto-atendimento-page/ponto-atendimento-page.component';
import { BuscaPageComponent } from './busca-page/busca-page.component';
import { CategoriaPageComponent } from './categoria-page/categoria-page.component';
import { CadastroMedicoComponent } from './cadastro-medico/cadastro-medico.component';
import { CadastroEspecialidadesComponent } from './cadastro-especialidades/cadastro-especialidades.component';
import { ConfiguracoesPlanosComponent } from './configuracoes-planos/configuracoes-planos.component';
import { ConfiguracoesPontoAtendimentoComponent } from './configuracoes-ponto-atendimento/configuracoes-ponto-atendimento.component';
import { DashboardPontoAtendimentoComponent } from './dashboard-ponto-atendimento/dashboard-ponto-atendimento.component';

const routes: Routes = [
  { path: 'login', component: LoginPageComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomePageComponent },
  { path: 'exemplo', component: ExemploPageComponent, canActivate: [AuthGuard] },
  { path: 'ponto-atendimento/:id', component: PontoAtendimentoPageComponent },
  { path: 'ponto-atendimento/:id/agendar', component: MarcacaoPageComponent },
  { path: 'busca', component: BuscaPageComponent },
  { path: 'categoria', component: CategoriaPageComponent },
  { path: 'esqueci-a-senha', component: EsqueciASenhaComponent },
  { path: 'cadastro-usuario', component: CadastroUsuarioComponent },
  { path: 'dashboard-ponto-atendimento', component: DashboardPontoAtendimentoComponent},
  { path: 'cadastro-medico/:key', component: CadastroMedicoComponent },
  { path: 'cadastro-especialidades', component: CadastroEspecialidadesComponent },
  { path: 'configuracoes-planos', component: ConfiguracoesPlanosComponent },
  { path: 'configuracoes-ponto-atendimento', component: ConfiguracoesPontoAtendimentoComponent },
  // { 
  //   path: 'dashboard-ponto-atendimento', 
  //   component: DashboardPontoAtendimentoComponent, 
  //   children: [
  //     { path: 'cadastro-medico', component: CadastroMedicoComponent },
  //     { path: 'cadastro-especialidades', component: CadastroEspecialidadesComponent },
  //     { path: 'configuracoes-planos', component: ConfiguracoesPlanosComponent },
  //     { path: 'configuracoes-ponto-atendimento', component: ConfiguracoesPontoAtendimentoComponent }
  //   ]
  // }
  { path: 'configuracoes', component: ConfiguracoesPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
