import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoriasService } from '../_services/categorias.service';
import { PontoAtendimentoService } from '../_services/ponto-atendimento.service';
import { Categoria } from '../_models/categoria';
import { PontoAtendimento } from '../_models/ponto-atendimento';

@Component({
  selector: 'app-categoria-page',
  templateUrl: './categoria-page.component.html',
  styleUrls: ['./categoria-page.component.css']
})
export class CategoriaPageComponent implements OnInit {
  categoriaAtual: any = new Categoria;
  pontosAtendimento = new Array<PontoAtendimento>();
  categorias: any = new Array<Categoria>();

  constructor(private router: ActivatedRoute,
              private pontoAtendimento: PontoAtendimentoService,
              private categoriaService: CategoriasService ) { }

  ngOnInit() {
    this.router.queryParams
      .subscribe(params => {
        if (params['selecionada']) {
          this.pontoAtendimento.getByQuery({ selecionada: params['selecionada'] })
            .then(pontosAtendimento => {
              console.log('retorno de ponto atendimentos ', pontosAtendimento);
              if (pontosAtendimento) {
                this.pontosAtendimento = pontosAtendimento.lista;
              }
            });

          this.categoriaService.getById(params['selecionada'])
            .subscribe(categoria => {
              console.log('retorno de categoria ', categoria);
              if (categoria) {
                this.categoriaAtual = categoria;
              }
            });
        }
      });

    this.categoriaService.getAll()
      .subscribe(categorias => {
        console.log('retorno de categorias ', categorias);
        if (categorias) {
          this.categorias = categorias;
        }
      });
  }

}
