import { Component, OnInit } from '@angular/core';
import { User } from '../_models/usuario';
import { AuthService } from '../_services/auth.service';
import { PontoAtendimento } from '../_models/ponto-atendimento';
import { Router } from '@angular/router';
import { GeocodeService } from '../_services/geocode.service';
import { PontoAtendimentoService } from '../_services/ponto-atendimento.service';
import { Papel } from '../_models/papel';

@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.css']
})
export class CadastroUsuarioComponent implements OnInit {
  mostrarSegundoForm = false;
  usuario = new User();
  pontoAtendimento = new PontoAtendimento();

  constructor(private router: Router,
              private geocodeService: GeocodeService,
              private pontoAtendimentoService: PontoAtendimentoService,
              private authService: AuthService) {}

  ngOnInit() {
    this.authService.depoisDeCriarConta
    .subscribe(() => {
      this.router.navigate(['/home']);
    });

    this.geocodeService.onGetAddress
      .subscribe(data => {
        if (data) {
          this.pontoAtendimento.localizacao.cidade = data.localidade;
          this.pontoAtendimento.localizacao.logradouro = data.logradouro;
          this.pontoAtendimento.localizacao.estado = data.uf;
          this.pontoAtendimento.localizacao.bairro = data.bairro;
          this.pontoAtendimento.localizacao.cep = data.cep;
          
          const end = this.pontoAtendimento.localizacao.logradouro + ', ' + this.pontoAtendimento.localizacao.numero + ', ' + this.pontoAtendimento.localizacao.bairro + ' - ' + this.pontoAtendimento.localizacao.cidade + ', ' + this.pontoAtendimento.localizacao.estado;

          this.getGeolocationByAddress(end);
        }

      });

    this.geocodeService.onGetGeoCode
    .subscribe(data => {
      console.log(data)
        if (data.results) {
          if(data.results[0].geometry){
            this.pontoAtendimento.localizacao.lat = data.results[0].geometry.location.lat;
            this.pontoAtendimento.localizacao.long = data.results[0].geometry.location.lng;
            this.pontoAtendimento.localizacao.enderecoCompleto = data.results[0].formatted_address;
          }
        }
      });


  }

  changeForm(hasSegundo) {
    this.mostrarSegundoForm = hasSegundo;
  }

  cadastrar() {
    const senha = this.usuario.senha;
    const email = this.usuario.email;

    

    // verificar se já existe o usuario e clinica

    if(this.mostrarSegundoForm) {
      this.authService.criarConta(email, senha, this.usuario, this.pontoAtendimento);
    } else {
      this.authService.criarConta(email, senha, this.usuario);
    }
  }

  getAddressByCep() {
    const cep = this.pontoAtendimento.localizacao.cep.split(' ').join('+');
    if (cep.length >= 8) {
      this.geocodeService.getAddress(this.pontoAtendimento.localizacao.cep);
    }
  }

  getGeolocationByAddress(end){
    this.geocodeService.getGeoCode(end);
  }
}
