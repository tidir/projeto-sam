import { Router } from '@angular/router';
import { UsuarioService } from './../_services/usuario.service';
import { AuthService } from './../_services/auth.service';
import { Usuario } from './../../../../SAM-app/src/_models/usuario';
import { UnsubscribeAll } from './../_utils/unsubscribe-all';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-configuracoes-page',
  templateUrl: './configuracoes-page.component.html',
  styleUrls: ['./configuracoes-page.component.css']
})
export class ConfiguracoesPageComponent implements OnInit {

  usuario = {};

  private _unsubscribe = new UnsubscribeAll();

  constructor(
  private authService: AuthService,
  private usuarioService: UsuarioService,
  private router: Router,
  private toastrService: ToastrService) {

      }

  ngOnInit() {
    this.usuario = Object.create(this.authService.usuario);
  }
  onNgSubmit() {
    this.usuarioService.put(this.usuario);
    this.authService._recuperarInfoUsuario(this.usuario);
    this.router.navigate(['/home']);
    this.toastrService.success('Alteração Realizada Com Sucesso');
  }

}
