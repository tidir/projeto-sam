import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPontoAtendimentoComponent } from './lista-ponto-atendimento.component';

describe('ListaPontoAtendimentoComponent', () => {
  let component: ListaPontoAtendimentoComponent;
  let fixture: ComponentFixture<ListaPontoAtendimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPontoAtendimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPontoAtendimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
