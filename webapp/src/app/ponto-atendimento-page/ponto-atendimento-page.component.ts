import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PontoAtendimentoService } from '../_services/ponto-atendimento.service';
import { PontoAtendimento } from '../_models/ponto-atendimento';

@Component({
  selector: 'app-ponto-atendimento-page',
  templateUrl: './ponto-atendimento-page.component.html',
  styleUrls: ['./ponto-atendimento-page.component.css']
})
export class PontoAtendimentoPageComponent implements OnInit {
  pontoAtendimento = new PontoAtendimento;

  constructor(private router: ActivatedRoute, private pontoAtendimentoService: PontoAtendimentoService) { }

  ngOnInit() {
    this.router.params
      .subscribe(params => {
        if (params) {
          const id = params['id'];
          this.pontoAtendimentoService.getById(id)
            .subscribe(dados => {
              if (dados) {
                this.pontoAtendimento = dados;
              }
            });
        }
      });
  }

}
