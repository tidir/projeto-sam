import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PontoAtendimentoPageComponent } from './ponto-atendimento-page.component';

describe('PontoAtendimentoPageComponent', () => {
  let component: PontoAtendimentoPageComponent;
  let fixture: ComponentFixture<PontoAtendimentoPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PontoAtendimentoPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PontoAtendimentoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
