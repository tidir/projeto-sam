import { Component, OnInit } from '@angular/core';
import { Medicos } from '../_models/medicos';
import { MedicosService } from '../_services/medicos.service';
import { User } from '../_models/usuario';
import { AuthService } from '../_services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { PontoAtendimentoService } from '../_services/ponto-atendimento.service';
import { PontoAtendimento } from '../_models/ponto-atendimento';
import { Especialidades } from '../_models/especialidades';
import { EspecialidadesService } from '../_services/especialidades.service';

@Component({
  selector: 'app-cadastro-medico',
  templateUrl: './cadastro-medico.component.html',
  styleUrls: ['./cadastro-medico.component.css']
})
export class CadastroMedicoComponent implements OnInit {

  medico = new Medicos();
  usuario = new User();
  pontoAtendimentoList = new Array<PontoAtendimento>();
  pontoAtendimento = new PontoAtendimento();
  especialidadesList = new Array<Especialidades>();
  keyPontoAtendimento: any;

  constructor(private medicoService: MedicosService,
              private route: ActivatedRoute,
              private pontoAtendimentoService: PontoAtendimentoService,
              private especialidadesService: EspecialidadesService,
              private authService: AuthService) { }

  ngOnInit() {
    this.route.params
      .subscribe(params => {
        if(params['key']) {
          this.keyPontoAtendimento = params['key'];
          this.pontoAtendimentoService.getPontoAtendimentorByKey(this.keyPontoAtendimento);
          console.log(this.keyPontoAtendimento);
          
        }
      });

    this.pontoAtendimentoService.afterGetPontoAtendimentorByKey
      .subscribe(data => {
        if (data) {
          this.pontoAtendimento = data;
        }
      });

    this.pontoAtendimentoService.afterGetPontoAtendimento
      .subscribe(data => {
        if (data) {
          this.pontoAtendimentoList = data;
        }
      });

    this.especialidadesService.afterGetEspecialidades
      .subscribe(data => {
        if (data) {
          this.especialidadesList = data;
        }
      });

    this.especialidadesService.getEspecialidades();
    this.pontoAtendimentoService.getPontoAtendimento();
  }

  cadastrarMedico() {
    let senha = 'medico1234';
    this.usuario.papel.nivel = 5;
    this.usuario.papel.nome = 'Médico';

    this.authService.criarConta(this.usuario.email, senha, this.usuario);

    this.medico.usuario = this.usuario;
    this.medicoService.postMedico(this.medico);

    // if (this.pontoAtendimento.uidMedicos.length === 0 || !this.pontoAtendimento.uidMedicos) {
    //   this.pontoAtendimento.uidMedicos = [];
    //   this.pontoAtendimento.uidMedicos.push(this.medico.uid);
    // } else {
    //   this.pontoAtendimento.uidMedicos.push(this.medico.uid);
    // }

    // this.pontoAtendimentoService.putPontoAtendimento(this.pontoAtendimento, this.pontoAtendimento.id);
  }

}
