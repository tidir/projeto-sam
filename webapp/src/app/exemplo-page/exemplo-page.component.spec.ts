import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExemploPageComponent } from './exemplo-page.component';

describe('ExemploPageComponent', () => {
  let component: ExemploPageComponent;
  let fixture: ComponentFixture<ExemploPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExemploPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExemploPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
