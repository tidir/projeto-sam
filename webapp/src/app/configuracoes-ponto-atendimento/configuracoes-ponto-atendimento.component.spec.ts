import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiguracoesPontoAtendimentoComponent } from './configuracoes-ponto-atendimento.component';

describe('ConfiguracoesPontoAtendimentoComponent', () => {
  let component: ConfiguracoesPontoAtendimentoComponent;
  let fixture: ComponentFixture<ConfiguracoesPontoAtendimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfiguracoesPontoAtendimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiguracoesPontoAtendimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
