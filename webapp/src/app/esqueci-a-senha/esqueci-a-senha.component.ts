import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../_services/auth.service';
import { UnsubscribeAll } from '../_utils/unsubscribe-all';

@Component({
  selector: 'app-esqueci-a-senha',
  templateUrl: './esqueci-a-senha.component.html',
  styleUrls: ['./esqueci-a-senha.component.css']
})
export class EsqueciASenhaComponent implements OnInit {
  private _unsubscribe = new UnsubscribeAll();

  mensagem: string;
  user: any = {};
  @ViewChild('meuFormulario') f: NgForm;

  constructor(private authService: AuthService) {}

  ngOnInit() {

  }

  enviarEmail() {
    this.authService.resetarSenha(this.user.email);
  }
}
