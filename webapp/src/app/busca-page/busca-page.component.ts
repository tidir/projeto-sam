import { PontoAtendimento } from './../_models/ponto-atendimento';
import { PontoAtendimentoService } from './../_services/ponto-atendimento.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { delay } from 'rxjs/operator/delay';



@Component({
  selector: 'app-busca-page',
  templateUrl: './busca-page.component.html',
  styleUrls: ['./busca-page.component.css']
})
export class BuscaPageComponent implements OnInit {

 pontosAtendimento: Array<PontoAtendimento> = new Array<PontoAtendimento>();
 nadaEncontrado = false;


  // tslint:disable-next-line:max-line-length
  constructor(private router: ActivatedRoute, private pontosAtendimentoService: PontoAtendimentoService, private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinnerService.show();
    this.router.queryParams
      .subscribe(params => {
        if (params) {
        this.pontosAtendimentoService.getByQuery(params)
        .then((dados) => {
          setTimeout( () => {
            this.pontosAtendimento = dados;
            this.spinnerService.hide();
            if (this.pontosAtendimento.length === 0) {
              this.nadaEncontrado = true;
            }
          }, 2000 );
        });
        }
      });
  }

}
