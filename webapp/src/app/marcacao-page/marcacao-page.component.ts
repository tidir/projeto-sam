import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { EspecialidadesService } from '../_services/especialidades.service';
import { Especialidades } from '../_models/especialidades';

@Component({
  selector: 'app-marcacao-page',
  templateUrl: './marcacao-page.component.html',
  styleUrls: ['./marcacao-page.component.css']
})
export class MarcacaoPageComponent implements OnInit {
  hoje: NgbDateStruct;

  agendamento: any = {};
  especialidades = Array<Especialidades>();

  constructor(private especialidadeService: EspecialidadesService) { }

  ngOnInit() {
    const agora = new Date();
    this.hoje = {
      day: agora.getDate(),
      month: agora.getMonth() + 1,
      year: agora.getFullYear()
    };

    this.especialidadeService.afterGetEspecialidades
      .subscribe(especialidades => {
        if (especialidades) {
          this.especialidades = especialidades;
        }
      });

    this.especialidadeService.getEspecialidades();
  }

  marcarConsulta() {
    alert('Consulta agendada com sucesso');
  }

}
