import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  Router
} from '@angular/router';
import { PontoAtendimentoService } from '../_services/ponto-atendimento.service';
import { PontoAtendimento } from '../_models/ponto-atendimento';
import { CategoriasService } from '../_services/categorias.service';
import { Categoria } from '../_models/categoria';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit, OnDestroy {
  fundo = 'bg-1';
  contador = 0;
  interval;
  pesquisa = { q: '', plano: 'todos' };
  pontosAtendimento = new Array<PontoAtendimento>();
  categorias: any = new Array<Categoria>();

  constructor(private router: Router,
              private pontoAtendimentoService: PontoAtendimentoService,
              private categoriaService: CategoriasService) { }

  ngOnInit() {
    this.contador = 0;
    this.interval = setInterval( () => {
      if (this.contador === 2000) {
        if (this.fundo !== 'bg-2') {
          this.fundo = 'bg-2';
        } else {
          this.fundo = 'bg-1';
        }
        this.contador = 0;
      }

      this.contador++;
    }, 1);

    this.categoriaService.getAll()
      .subscribe(categorias => {
        if (categorias) {
          this.categorias = categorias;
        }
      });


    this.pontoAtendimentoService.getAll()
      .then(dados => {
        if (dados) {
          this.pontosAtendimento = dados;
        }
      });
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  buscar() {
    this.router.navigate(['busca'], {
      queryParams: this.pesquisa
    });
  }

}
