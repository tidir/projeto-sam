import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaMarcacoesComponent } from './lista-marcacoes.component';

describe('ListaMarcacoesComponent', () => {
  let component: ListaMarcacoesComponent;
  let fixture: ComponentFixture<ListaMarcacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaMarcacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaMarcacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
