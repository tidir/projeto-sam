import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiguracoesPlanosComponent } from './configuracoes-planos.component';

describe('ConfiguracoesPlanosComponent', () => {
  let component: ConfiguracoesPlanosComponent;
  let fixture: ComponentFixture<ConfiguracoesPlanosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfiguracoesPlanosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiguracoesPlanosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
