import { Especialidades } from "./especialidades";
import { User } from "./usuario";

export class Medicos {
    uid: string;
    usuario: User;
    crm: string;
    especialidades: Array<Especialidades>;

    constructor() {
        this.usuario = new User();
        this.especialidades = new Array<Especialidades>();
    }
}
