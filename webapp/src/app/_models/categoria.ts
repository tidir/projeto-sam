import { AbastractFirebaseModel } from './abstract-firebase-model';

export class Categoria extends AbastractFirebaseModel {
  nome: string;
}
