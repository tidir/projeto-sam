export class Localizacao {
  cidade: string;
  logradouro: string;
  estado: string;
  bairro: string;
  cep: string;
  complemento: string;
  numero: string;
  enderecoCompleto: string;
  lat: number;
  long: number;
}
