import { AbastractFirebaseModel } from './abstract-firebase-model';
import { Localizacao } from './localizacao';
import { Plano } from './plano';
import { Medicos } from './medicos';

export class PontoAtendimento extends AbastractFirebaseModel {
  nome: string;
  descricao: string;
  planos: Array<Plano>;
  telefone: string;
  email: string;
  cnpj: string;
  localizacao: Localizacao;
  dono: string;
  uidMedicos: Array<string>;

  constructor() {
    super();
    this.planos = new Array<Plano>();
    this.localizacao = new Localizacao;
  }
}
