export class Agendamento {
  clinica: string;
  paciente: string;
  responsavel: string;
  plano: string;
  status: string;
  data: string;
  hora: string;
  observacao: string;
}
