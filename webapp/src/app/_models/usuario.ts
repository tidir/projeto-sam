import { AbastractFirebaseModel } from './abstract-firebase-model';
import { Papel } from './papel';

export class User extends AbastractFirebaseModel {
  uid: string;
  nome: string;
  email: string;
  senha: string;
  img_url: string;
  ativo: boolean;
  dataNascimento: string;
  cpf: string;
  tel: string;
  sexo: string;
  papel: Papel;

  constructor() {
    super();
    this.papel = new Papel('user', 0);
  }
}
