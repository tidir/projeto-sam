import { UsuarioService } from './_services/usuario.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';

import { AgmCoreModule } from '@agm/core';
import { AppComponent } from './app.component';
import { RodapeComponent } from './_components/rodape/rodape.component';
import { AppRoutingModule } from './app-routing.module';
import { HomePageComponent } from './home-page/home-page.component';
import { CabecarioComponent } from './_components/cabecario/cabecario.component';
import { ExemploPageComponent } from './exemplo-page/exemplo-page.component';
import { AuthService } from './_services/auth.service';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { ToastrModule } from 'ngx-toastr';
import { ExemploService } from './_services/exemplo.service';
import { DatabaseAdpter } from './_utils/dabase-adpter';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthGuard } from './_guard/auth.guard';
import { EsqueciASenhaComponent } from './esqueci-a-senha/esqueci-a-senha.component';
import { PacienteService } from './_services/paciente.service';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';
import { MarcacaoPageComponent } from './marcacao-page/marcacao-page.component';
import { PontoAtendimentoService } from './_services/ponto-atendimento.service';
import { PontoAtendimentoPageComponent } from './ponto-atendimento-page/ponto-atendimento-page.component';
import { BuscaPageComponent } from './busca-page/busca-page.component';
import { CategoriaPageComponent } from './categoria-page/categoria-page.component';
import { CategoriasService } from './_services/categorias.service';
import { GeocodeService } from './_services/geocode.service';
import { DashboardPontoAtendimentoComponent } from './dashboard-ponto-atendimento/dashboard-ponto-atendimento.component';
import { CadastroMedicoComponent } from './cadastro-medico/cadastro-medico.component';
import { ConfiguracoesPontoAtendimentoComponent } from './configuracoes-ponto-atendimento/configuracoes-ponto-atendimento.component';
import { ConfiguracoesPlanosComponent } from './configuracoes-planos/configuracoes-planos.component';
import { CadastroEspecialidadesComponent } from './cadastro-especialidades/cadastro-especialidades.component';
import { MedicosService } from './_services/medicos.service';
import { EspecialidadesService } from './_services/especialidades.service';
import { ConfiguracoesPageComponent } from './configuracoes-page/configuracoes-page.component';
import { ListaMedicosComponent } from './lista-medicos/lista-medicos.component';
import { ListaEspecialidadesComponent } from './lista-especialidades/lista-especialidades.component';
import { ListaPontoAtendimentoComponent } from './lista-ponto-atendimento/lista-ponto-atendimento.component';
import { ListaMarcacoesComponent } from './lista-marcacoes/lista-marcacoes.component';
import { MarcacaoDetalheComponent } from './marcacao-detalhe/marcacao-detalhe.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

@NgModule({
  declarations: [
    AppComponent,
    RodapeComponent,
    HomePageComponent,
    CabecarioComponent,
    ExemploPageComponent,
    LoginPageComponent,
    EsqueciASenhaComponent,
    CadastroUsuarioComponent,
    MarcacaoPageComponent,
    PontoAtendimentoPageComponent,
    BuscaPageComponent,
    CategoriaPageComponent,
    DashboardPontoAtendimentoComponent,
    CadastroMedicoComponent,
    ConfiguracoesPontoAtendimentoComponent,
    ConfiguracoesPlanosComponent,
    CadastroEspecialidadesComponent,
    ConfiguracoesPageComponent,
    ListaMedicosComponent,
    ListaEspecialidadesComponent,
    ListaPontoAtendimentoComponent,
    ListaMarcacoesComponent,
    MarcacaoDetalheComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    Ng4LoadingSpinnerModule.forRoot(),
    AppRoutingModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDr0jr0Nw1HezUjvbzv-RYntm2JP2FIQ4o'
    }),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    NgbModule.forRoot()
  ],
  providers: [
    DatabaseAdpter,
    AuthService,
    CategoriasService,
    ExemploService,
    AuthGuard,
    PacienteService,
    UsuarioService,
    GeocodeService,
    MedicosService,
    EspecialidadesService,
    PontoAtendimentoService,
    { provide: LOCALE_ID, useValue: 'pt'  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
