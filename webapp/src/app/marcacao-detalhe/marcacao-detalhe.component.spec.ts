import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcacaoDetalheComponent } from './marcacao-detalhe.component';

describe('MarcacaoDetalheComponent', () => {
  let component: MarcacaoDetalheComponent;
  let fixture: ComponentFixture<MarcacaoDetalheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcacaoDetalheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcacaoDetalheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
