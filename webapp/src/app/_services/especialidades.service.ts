import { Injectable, Output, EventEmitter } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { Http } from '@angular/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { Especialidades } from '../_models/especialidades';

@Injectable()
export class EspecialidadesService {

  @Output() afterGetEspecialidades = new EventEmitter();
  @Output() afterPostEspecialidade = new EventEmitter();
  @Output() afterPutEspecialidade = new EventEmitter();
  @Output() afterDeleteEspecialidade = new EventEmitter();

  readonly STORAGE_KEY = '/especialidades/';
  readonly API = environment.api + this.STORAGE_KEY;

  constructor(private http: Http,
              private afDB: AngularFireDatabase) { }

  getEspecialidades() {
    this.afDB.database.ref(this.STORAGE_KEY)
      .once('value', data => {
        let arr = [];

        if (data.val()) {
          const val = data.val();
          arr = Object.keys(val)
            .map( (key) => {
              val[key]['key'] = key;
              return val[key];
            });
        }
        this.afterGetEspecialidades.emit(arr);
      });
  }

  postEspecialidades(especialidade: Especialidades) {
    this.afDB.database.ref(this.STORAGE_KEY)
      .push(especialidade)
      .then(data => {
        this.afterPostEspecialidade.emit(data);
      });
  }

  putEspecialidades(especialidade: Especialidades, key: string) {
    this.afDB.database.ref(this.STORAGE_KEY + key)
      .update(especialidade)
      .then(data => {
        this.afterPutEspecialidade.emit(data);
      });
  }

  deleteEspecialidades(key: string) {
    this.afDB.database.ref(this.STORAGE_KEY + key)
      .remove()
      .then(data => {
        this.afterDeleteEspecialidade.emit(data);
      });
  }

}
