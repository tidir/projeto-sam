import { Injectable, Output, EventEmitter } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { Http } from '@angular/http';
import { PontoAtendimento } from '../_models/ponto-atendimento';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class PontoAtendimentoService {

  @Output() afterGetPontoAtendimento = new EventEmitter();
  @Output() afterGetPontoAtendimentorByKey = new EventEmitter();
  @Output() afterPostPontoAtendimento = new EventEmitter();
  @Output() afterPutPontoAtendimento = new EventEmitter();

  readonly STORAGE_KEY = '/pontos-atendimento/';
  readonly API = environment.api + this.STORAGE_KEY;

  constructor(private http: Http,
              private afDB: AngularFireDatabase) { }

  getAll() {
    return this.http.get(this.API)
      .map(dados => dados.json())
      .toPromise();
  }

  getByQuery(object) {
    let query = '?';

    Object.keys(object)
      .forEach(key => {
        query += `${key}=${object[key]}&`;
      });

    return this.http.get(this.API + query)
          .map(dados => dados.json())
          .toPromise();
  }

  getById(id: string) {
    return this.http.get(`${this.API}${id}`)
      .map(dados => dados.json());
  }

  getPontoAtendimento() {
    this.afDB.database.ref(this.STORAGE_KEY)
      .once('value', data => {
        let arr = [];

        if (data.val()) {
          const val = data.val();
          arr = Object.keys(val)
            .map( (key) => {
              val[key]['key'] = key;
              return val[key];
            });
        }
        this.afterGetPontoAtendimento.emit(arr);
      });
  }

  getPontoAtendimentorByKey(key: string) {
    this.afDB.object(this.STORAGE_KEY + key)
      .valueChanges()
      .subscribe(data => {
        data['key'] = key;
        this.afterGetPontoAtendimentorByKey.emit(data);
      });
  }

  postPontoAtendimento(pontoAtendimento: PontoAtendimento) {
    this.afDB.database.ref(this.STORAGE_KEY)
      .push(pontoAtendimento)
      .then(data => {
        this.afterPostPontoAtendimento.emit(data);
      });
  }

  putPontoAtendimento(pontoAtendimento: PontoAtendimento, key: string) {
    this.afDB.database.ref(this.STORAGE_KEY + key)
      .update(pontoAtendimento)
      .then(data => {
        this.afterPutPontoAtendimento.emit(data);
      });
  }

}
