import { Injectable } from '@angular/core';
import { DatabaseAdpter } from '../_utils/dabase-adpter';
import { Categoria } from '../_models/categoria';

@Injectable()
export class CategoriasService {

  readonly STORAGE_KEY = '/categorias/';

  constructor(private db: DatabaseAdpter) { }

  getAll() {
    return this.db.get(this.STORAGE_KEY);
  }

  getById(id) {
    return this.db.getById(this.STORAGE_KEY, id);
  }

  post(categoria: Categoria) {
    return this.db.post(categoria, this.STORAGE_KEY);
  }

  put(categoria: Categoria) {
    return this.db.put(categoria, this.STORAGE_KEY);
  }

  delete(categoria: Categoria) {
    this.db.delete(categoria.id, this.STORAGE_KEY);
  }


}
