import { Injectable, Input, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class GeocodeService {

  @Input() onGetGeoCode = new EventEmitter();
  @Input() onGetAddress = new EventEmitter();

  constructor(private http: Http) { }

  getAddress(cep: string){

    this.http.get(`https://viacep.com.br/ws/${cep}/json/`)
    .subscribe(r => {
      const data = r.json();
      this.onGetAddress.emit(data);
    });

  }

  getGeoCode(geo: string){

    this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${geo}&key=AIzaSyBX3f9mxvu3yH-CN0okZnuvxhTIX0xPK2s`)
    .subscribe(r => {
      const data = r.json();
      this.onGetGeoCode.emit(data);
    });

  }

}
