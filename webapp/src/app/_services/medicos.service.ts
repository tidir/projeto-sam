import { Injectable, Output, EventEmitter } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { Http } from '@angular/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { Medicos } from '../_models/medicos';

@Injectable()
export class MedicosService {

  @Output() afterGetMedicoByKey = new EventEmitter();
  @Output() afterGetMedicos = new EventEmitter();
  @Output() afterPostMedico = new EventEmitter();
  @Output() afterPutMedico = new EventEmitter();
  @Output() afterDeleteMedico = new EventEmitter();

  readonly STORAGE_KEY = '/medicos/';
  readonly API = environment.api + this.STORAGE_KEY;

  constructor(private http: Http,
              private afDB: AngularFireDatabase) { }

  getMedicotByKey(key: string) {
    this.afDB.object(this.STORAGE_KEY + key)
      .valueChanges()
      .subscribe(data => {
        data['key'] = key;
        this.afterGetMedicoByKey.emit(data);
      });
  }

  getMedicos() {
    this.afDB.database.ref(this.STORAGE_KEY)
      .once('value', data => {
        let arr = [];

        if (data.val()) {
          const val = data.val();
          arr = Object.keys(val)
            .map( (key) => {
              val[key]['key'] = key;
              return val[key];
            });
        }
        this.afterGetMedicos.emit(arr);
      });
  }

  postMedico(medico: Medicos) {
    this.afDB.database.ref(this.STORAGE_KEY)
      .push(medico)
      .then(data => {
        this.afterPostMedico.emit(data);
      });
  }

  putMedico(medico: Medicos, key: string) {
    this.afDB.database.ref(this.STORAGE_KEY + key)
      .update(medico)
      .then(data => {
        this.afterPutMedico.emit(data);
      });
  }

  deleteMedico(key: string) {
    this.afDB.database.ref(this.STORAGE_KEY + key)
      .remove()
      .then(data => {
        this.afterDeleteMedico.emit(data);
      });
  }

}
