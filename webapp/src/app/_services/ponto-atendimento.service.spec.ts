import { TestBed, inject } from '@angular/core/testing';

import { PontoAtendimentoService } from './ponto-atendimento.service';

describe('PontoAtendimentoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PontoAtendimentoService]
    });
  });

  it('should be created', inject([PontoAtendimentoService], (service: PontoAtendimentoService) => {
    expect(service).toBeTruthy();
  }));
});
