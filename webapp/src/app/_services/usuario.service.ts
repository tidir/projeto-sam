import { User } from './../_models/usuario';
import { Injectable } from '@angular/core';
import { DatabaseAdpter } from '../_utils/dabase-adpter';
import { Exemplo } from '../_models/exemplo';

@Injectable()
export class UsuarioService {
  readonly STORAGE_KEY = '/usuarios/';

  constructor(private db: DatabaseAdpter) {}

  getAll() {
    return this.db.get(this.STORAGE_KEY);
  }

  getById(id) {
    return this.db.getById(id, this.STORAGE_KEY);
  }

  post(usuario: any) {
    return this.db.post(usuario, this.STORAGE_KEY);
  }

  put(usuario: any) {
    return this.db.put(usuario, this.STORAGE_KEY);
  }

  delete(usuario: any) {
    this.db.delete(usuario.id, this.STORAGE_KEY);
  }
}
