const admin = require('firebase-admin'),
      functions = require('firebase-functions'),
      firebase = admin.initializeApp(functions.config().firebase);

const tools = {
  admin: admin,
  functions: functions,
  firebase: firebase
};

module.exports = tools;
