'use strict';
const express = require('express'),
      cookieParser = require('cookie-parser')(),
      cors = require('cors')({ origin: true }),
      app = express(),
      validateIdToken = require('../middleware/authentication'),
      errorHandler = require('../middleware/error-handler');

const pontoAtendimento = require('../controllers/ponto-atendimento');
const exemplo = require('../controllers/exemplo');
const agendamento = require('../controllers/agendamento');
const planos = require ('../controllers/planos.js');
const categorias = require ('../controllers/categorias.js');

// import middlewares here //
app.use(cors);
app.use(cookieParser);


app.get('/exemplo', exemplo.getAll);
app.get('/exemplo/:id', exemplo.getById);

app.get('/pontos-atendimento', pontoAtendimento.getAll);
app.get('/pontos-atendimento/:id', pontoAtendimento.getById);
app.put('/pontos-atendimento/', validateIdToken, pontoAtendimento.put);

app.get('/planos', planos.getAll);
app.get('/categorias', categorias.getAll);


// error handler to solve stack and error message
app.use(errorHandler);


module.exports = app;
