const admin = require('../lib/firebase').admin;

const validateFirebaseIdToken = (req, res, next) => {

  if (
      (!req.headers.authorization ||
          !req.headers.authorization.startsWith('Bearer ')) &&
      !req.cookies.__session
  ) {
      res.status(403).send('Unauthorized');
      return;
  }

  let idToken;
  if (
      req.headers.authorization &&
      req.headers.authorization.startsWith('Bearer ')
  ) {
      idToken = req.headers.authorization.split('Bearer ')[1];
  } else {;
      idToken = req.cookies.__session;
  }
  admin
      .auth()
      .verifyIdToken(idToken)
      .then(decodedIdToken => {
          req.user = decodedIdToken;
          next();
      })
      .catch(error => {
          res.status(403).send('Unauthorized');
      });
};

module.exports = validateFirebaseIdToken;
