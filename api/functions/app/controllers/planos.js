const firebase = require('../lib/firebase').firebase;

class Planos {

  getAll(req, res) {
    firebase.database().ref('planos')
      .once('value', planoSnap => {
          const planos = planoSnap.val();
          console.log('teste');
          if(planos){
              res.send(planos);
          }else{
              res.send(404);
          }
      });
  }

}

module.exports = new Planos();
