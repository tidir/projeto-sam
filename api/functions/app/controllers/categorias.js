const firebase = require('../lib/firebase').firebase;

class Categorias {

  getAll(req, res) {
    firebase.database().ref('categorias')
      .once('value', categoriaSnap => {
          const categorias = categoriaSnap.val();
          console.log('teste');
          if(categorias){
              res.send(categorias);
          }else{
              res.send(404);
          }
      });
  }

}

module.exports = new Categorias();
