const firebase = require('../lib/firebase').firebase;

class PontoAtendimento {

  getAll(req, res) {

    let { q, plano } = req.query;

    let busca = firebase.database().ref('pontos-atendimento');

    if(q) {

        busca = busca.orderByChild('nome').equalTo(q);
    }

    busca.once('value', buscaSnap =>{
        const resultado = buscaSnap.val();
        let lista = [];

        if(resultado){

         lista = Object.keys(resultado)
          .map((key)=> {
            resultado[key].id = key;
            return resultado[key]
          });

        if(plano) {

        let filtro = lista
          .filter( pontoAtendimento => {
            let hasPlano = false;

            if(pontoAtendimento.planos) {
              pontoAtendimento.planos.forEach(pontoPlano => {
                if(pontoPlano === plano) hasPlano = true;
              });
            }

            return hasPlano;
          });

        }
      }
        res.send(lista)
    })

  }
  getById(req,res){

    const id = req.params.id;

    firebase.database().ref(`pontos-atendimento/${id}`)
    .once('value',pontoAtendimentoSnap => {
        const pontoAtendimento = pontoAtendimentoSnap.val();
        if(pontoAtendimento) {
          res.send(pontoAtendimento);
        } else {
          res.sendStatus(404);
        }
    });
  }

  put(req, res) {
    let body = req.body;

    firebase.database().ref(`ponto-atendimento/`)
      .orderByChild('dono')
      .equalTo(req.user.uid)
      .once('value', pontoAtendimento => {
        let pontosAtendimento = pontoAtendimento.val();

        if(pontosAtendimento) {
          let pontoAtendimento = {};
          Object.keys(pAtendimento)
            .map(key => {
              pontoAtendimento = pAtendimento[key];
              pontoAtendimento.key = key;
            });

          let merge = Object.assing({}, pontoAtendimento, body);

          firebase.database()
            .ref(`ponto-atendimento/${pontoAtendimento.key}`)
            .update(merge);

          res.send(merge)
        }

      });

  }
}

module.exports = new PontoAtendimento();
