const firebase = require('../lib/firebase').firebase;

class Exemplo{

    getAll(req,res){
        firebase.database().ref('exemplo')
        .once('value',exemploSnap =>{
            const exemplo = exemploSnap.val();
            res.send(exemplo);
        });
    }

    getById(req,res){

        const id = req.params.id;

        firebase.database().ref('exemplo/' + id)
        .once('value',exemploSnap =>{
            const exemplo = exemploSnap.val();
            res.send(exemplo);
        });
    }

}

module.exports = new Exemplo();