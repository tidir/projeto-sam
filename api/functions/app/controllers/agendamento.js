const firebase = require('../lib/firebase').firebase;

class Agendamento{

    getAll(req,res){
    
        let agendamento = [
          
            { id:'01',
              nomePaciente: 'Sandro Gomes',
              nomeMedico: 'Carlos Eduardo Gomes',
              especialidade:'Ortopedista',
              telefonePaciente: '(31) 3271-3389',
              dataAtendamento:'1525204676278',
              emailPaciente:'sandro@gmail.com',
              planoDeSaude: 'unimed',
              
          },
          { id:'02',
            nomePaciente: 'Gilberto Dias',
            nomeMedico: 'Carlos Eduardo Gomes',
            especialidade:'Ortopedista',
            telefonePaciente: '(31) 2556-1588',
            dataAtendamento:'"1524781911864"',
            emailPaciente:'gilDias@gmail.com',
            planoDeSaude: 'vitalis',
    
          },
          { id:'03',
            nomePaciente: 'Pedro Henrique',
            nomeMedico: 'Hugo Rubens Gomes',
            especialidade:'Pneomologista',
            telefonePaciente: '(31) 2458-3685',
            dataAtendamento:'"1526338689380"',
            emailPaciente:'Ph@gmail.com',
            planoDeSaude: 'MaterDei',
    
          },
          { id:'04',
            nomePaciente: 'Douglas Cunha',
            nomeMedico: 'Antonia Eduarda Caetano',
            especialidade:'Ortopedista',
            telefonePaciente: '(31) 2256-6358',
            dataAtendamento:'"1525204786102"',
            emailPaciente:'cunhadouglasg@gmail.com',
            planoDeSaude: 'Casu',
    
          },
          { id:'05',
            nomePaciente: 'Adalberto Dias',
            nomeMedico: 'Antonia Eduardo Caetano',
            especialidade:'Ortopedista',
            telefonePaciente: '(31) 2125-8452',
            dataAtendamento:'"1526338689380"',
            emailPaciente:'adalbertog@gmail.com',
            planoDeSaude: 'Promed',
     
          }
        ]
        res.send(agendamento);
      }

      post(req,res){
            const agendamento = req.body;

            res.send(agendamento);
      }
}

module.exports = new Agendamento();