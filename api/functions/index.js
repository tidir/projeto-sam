'use strict';

const api = require('./app/lib/express'),
      functions = require('firebase-functions');

exports.api = functions.https.onRequest(api);
