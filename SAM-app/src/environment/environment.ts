import { RequestOptions, Headers } from "@angular/http";
const token = localStorage.getItem('token');
const headers = new Headers();
headers.append('Content-Type', 'application/json');
headers.append('Authorization', `Bearer ${token}`);
export const options = new RequestOptions({ headers: headers });

export const environment = {
  firebaseConfig: {
    apiKey: "AIzaSyDdWzhvle7NVWsRoIo8OZRipGeGZ4AFuBE",
    authDomain: "pi-2018.firebaseapp.com",
    databaseURL: "https://pi-2018.firebaseio.com",
    projectId: "pi-2018",
    storageBucket: "pi-2018.appspot.com",
    messagingSenderId: "359476027854"
  },
  apiUrl: 'https://us-central1-pi-2018.cloudfunctions.net/api/'
};


