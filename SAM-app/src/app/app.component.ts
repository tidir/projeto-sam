import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { AuthProvider } from '../providers/auth/auth';
import { PerfilPage } from '../pages/perfil/perfil';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage:any = HomePage;
  rootPage: any;
  version = 1;
  pages: Array<{ icon: string; title: string; component: any }>;
  @ViewChild('content') nav: NavController;

  constructor(platform: Platform, private menu: MenuController,
    statusBar: StatusBar, splashScreen: SplashScreen, public auth: AuthProvider) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      let logged = localStorage.getItem('logged');
      if (logged) {
        this.rootPage = HomePage;
        // this.rootPage = CuponsPage;
      } else {
        this.rootPage = LoginPage;
      }
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.pages = [
      { icon: 'md-home', title: 'Estabelecimentos', component: HomePage },
      { icon: 'md-person', title: 'Perfil', component: PerfilPage },
    ];
  }

  sair() {
    this.menu.close();
    this.auth.sair().then(() => {
      this.nav.setRoot(LoginPage);
    });
  }


  openPage(page: { title: string; component: any }) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    // this.nav.goToRoot(page.component);
    if (page.title == 'Perfil') {
      this.nav.push(page.component);
    } else {
      this.nav.setRoot(page.component);
    }
    // this.rootPage = page.component;
  }
}

