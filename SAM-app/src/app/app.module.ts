import { PerfilPageModule } from './../pages/perfil/perfil.module';
import { DetalheClinicaPageModule } from './../pages/detalhe-clinica/detalhe-clinica.module';
import { DetalheClinicaPage } from './../pages/detalhe-clinica/detalhe-clinica';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AuthProvider } from '../providers/auth/auth';
import { AngularFireModule } from 'angularfire2';
import { environment } from './../environment/environment';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import {
  AngularFireDatabaseModule,
  AngularFireDatabase
} from 'angularfire2/database';
import { LoginPageModule } from '../pages/login/login.module';
import { ClinicaProvider } from '../providers/clinica/clinica';
import { LoadingAlertProvider } from '../providers/loading-alert/loading-alert';
import { MapProvider } from '../providers/map/map';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';
import { Network } from '@ionic-native/network';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ContatoClinicaPageModule } from '../pages/contato-clinica/contato-clinica.module';
import { RecuperaSenhaPageModule } from '../pages/recupera-senha/recupera-senha.module';

@NgModule({
  declarations: [MyApp, HomePage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    LoginPageModule,
    DetalheClinicaPageModule,
    PerfilPageModule,
    HttpModule,
    HttpClientModule,
    RecuperaSenhaPageModule,
    ContatoClinicaPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, HomePage],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    AngularFireAuth,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    ClinicaProvider,
    LoadingAlertProvider,
    MapProvider,
    ConnectivityProvider,
    Network,
    Geolocation
  ]
})
export class AppModule { }
