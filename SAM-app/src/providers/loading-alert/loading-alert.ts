import { Injectable } from '@angular/core';
import { LoadingController, AlertController, Loading } from 'ionic-angular';

/*
  Generated class for the LoadingAlertProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoadingAlertProvider {
  // @Output() afterAlertCancelScheduling = new EventEmitter();

  loading: Loading;
  constructor(
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {
    console.log('Hello LoadingAlertProvider Provider');
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });
    this.loading.present();
    // return loading;
  }

  presentAlert(msg) {
    this.alertCtrl
      .create({
        message: msg,
        buttons: ['ok']
      })
      .present();
  }

  dismissLoad() {
    if (this.loading) {
      this.loading.dismissAll();
    }
    this.loading = null;
  }

  openDialog(obj: {
    title: string;
    msg: string;
    class: string;
    cancel: string;
    confirm: string;
  }): Promise<any> {
    let alert = this.alertCtrl.create({ enableBackdropDismiss: false });

    alert.setTitle(obj.title);
    alert.setMessage(obj.msg);
    alert.setCssClass(obj.class);
    return new Promise((resolve, reject) => {
      if (obj.cancel !== '') {
        alert.addButton({
          text: obj.cancel,
          role: 'cancel'
        });
      }

      alert.addButton({
        text: obj.confirm,
        role: 'ok',
        handler: () => {
          resolve();
        }
      });
      alert.present();
    });
  }

  alertCancelScheduling(obj: any): Promise<any> {
    let alert = this.alertCtrl.create();

    alert.setTitle(obj.title);
    alert.setMessage(obj.msg);
    alert.setCssClass(obj.class);

    alert.addButton({
      text: 'Não',
      role: 'cancel'
    });
    return new Promise((resolve, reject) => {
      alert.addButton({
        text: 'Sim',
        role: 'ok',
        handler: () => {
          // this.afterAlertCancelScheduling.emit(obj.dataCancelamento);
          resolve(obj.dataCancelamento);
        }
      });
      alert.present();
    });
  }
}
