import { Injectable, EventEmitter, Output } from '@angular/core';
import { environment, options } from '../../environment/environment';
import { Http } from '@angular/http';

const url = environment.apiUrl;
@Injectable()
export class ClinicaProvider {

  @Output() aposRecuperarPA = new EventEmitter();
  @Output() afterErrorGetClient = new EventEmitter();

  constructor(public http: Http) {
  }

  recuperaPA() {
    const apiURL = url + `pontos-atendimento`,
      apiURL3 = url + 'planos',
      apiURL2 = url + 'categorias';

    this.http
      .get(apiURL, options)
      .toPromise()
      .then(
        res => {
          // Success
          console.log('sucesso', res.json());
          const pA = res.json();
          if (pA) {
            this.http.get(apiURL3, options).toPromise().then(planosData => {
              let planos = [];
              if (planosData.json()) {
                this.http.get(apiURL2, options).toPromise().then(categoriasData => {
                  let categorias = []
                  if (categoriasData.json()) {
                    categorias = Object.keys(categoriasData.json()).map(key => {
                      return categoriasData.json()[key]
                    });
                    planos = Object.keys(planosData.json()).map(key => {
                      return planosData.json()[key]
                    });

                    console.log('categorias', categorias)
                    console.log('planos', planos);
                    const obj = {
                      pAtend: pA,
                      categorias: categorias,
                      planos: planos
                    }
                    this.aposRecuperarPA.emit(obj);
                  }
                }).catch(err => {
                  console.log(err);
                })
              }
            }).catch(err => {
              console.log(err);
            })
          }
        }
      ).catch(err => {

        console.log('erro', err);
      });
  }
  recuperaPlanosCategorias() {
  }


}
