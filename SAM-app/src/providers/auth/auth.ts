import { Injectable, Output, EventEmitter } from '@angular/core';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import { PontoAtendimento } from '../../_models/ponto-atendimento';
import { Papel } from '../../_models/papel';
import { Usuario } from '../../_models/usuario';

@Injectable()
export class AuthProvider {
  uid: string;
  authState: any;
  readonly STORAGE_KEY = '/usuarios/';
  readonly LOCALSTORAGE_KEY = 'logged';
  readonly LOCALSTORAGE_CURRENT = 'current';

  @Output() depoisDeLogar = new EventEmitter();
  @Output() erroLogin = new EventEmitter();
  @Output() depoisDeCriarConta = new EventEmitter();
  @Output() depoisDeDeslogar = new EventEmitter();
  @Output() depoisDeResetar = new EventEmitter();
  @Output() erroRecuperar = new EventEmitter();

  usuario = {};

  constructor(
    private afAuth: AngularFireAuth,
    private afDB: AngularFireDatabase
  ) {
    this.authState = this.afAuth.authState.subscribe(data => {
      if (data != null) {
        // const sb = this;
        data.getIdToken().then(function (token) {
          localStorage.setItem('token', token);
        });
        // this._recuperarInfoUsuario(data);
        localStorage.setItem('fbState', JSON.stringify(data));
        this.uid = data.uid;
      } else {
        localStorage.removeItem('fbState');
        localStorage.removeItem('key');
        localStorage.removeItem('token');
      }
    });
  }

  entrar(email: string, senha: string) {
    this.afAuth.auth
      .signInWithEmailAndPassword(email, senha)
      .then(usuario => {
        if (usuario) {
          localStorage.setItem(this.LOCALSTORAGE_KEY, 'true');
          this.afDB
            .object(`/usuarios/${usuario.uid}`)
            .valueChanges()
            .subscribe(snap => {
              this.usuario = snap;
              localStorage.setItem(
                this.LOCALSTORAGE_CURRENT,
                JSON.stringify(snap)
              );
              this.depoisDeLogar.emit(usuario);
            });
        }
      })
      .catch(err => {
        if (err) {
          switch (err.code) {
            case 'auth/wrong-password':
              err.message =
                'Não foi possível logar. \nCPF ou Senha incorreto(s). Tente novamente.';
              break;
            case 'auth/user-not-found':
              err.message =
                ' Não há registro de usuário correspondente a este email.';
              break;
            case 'auth/invalid-email':
              err.message = 'O endereço de e-mail está mal formatado.';
              break;
            case 'auth/network-request-failed':
              err.message =
                'Ocorreu um erro de rede (como tempo limite, conexão interrompida ou host inacessível).';
              break;

            default:
              err.message = 'consulte o administrador';
              break;
          }
          this.erroLogin.emit(err);
        }
      });
  }

  entrarComGoogle() {
    this.afAuth.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(usuario => {
        this._recuperarInfoUsuario(usuario);
        localStorage.setItem(this.LOCALSTORAGE_KEY, 'true');
        this.depoisDeLogar.emit();
      });
  }

  criarConta(email: string, senha: string, usuario: Usuario, clinica: PontoAtendimento | any | boolean = false) {
    this.afAuth.auth
      .createUserWithEmailAndPassword(email, senha)
      .then(user => {
        if (user) {
          if (clinica) {
            const updatesClinica = {};
            usuario.papel = new Papel('Administrador', 2);
            clinica.dono = user.uid;
            updatesClinica[
              'pontos-atendimento/' + this.afDB.createPushId()
            ] = clinica;
            this.afDB.app
              .database()
              .ref()
              .update(updatesClinica);
          }

          const updates = {},
            ref = this.STORAGE_KEY + user.uid;
          updates[ref] = usuario;
          delete updates[ref]['senha'];
          this.afDB.app
            .database()
            .ref()
            .update(updates);

          user
            .updateProfile({
              displayName: `${usuario.nome}`
            })
            .then(() => {
              this._recuperarInfoUsuario(user);
              this.depoisDeCriarConta.emit();
            });

          localStorage.setItem(this.LOCALSTORAGE_KEY, 'true');
        }
      })
      .catch(err => {
        if (err) {
          console.log(err);
        }
      });
  }

  resetarSenha(email: string) {
    this.afAuth.auth
      .sendPasswordResetEmail(email)
      .then(success => {
        this.depoisDeResetar.emit();
      })
      .catch(err => {
        if (err) {
          switch (err.code) {
            case 'auth/user-not-found':
              err.message =
                ' Não há registro de usuário correspondente a este email.';
              break;
            case 'auth/invalid-email':
              err.message = 'O endereço de e-mail está mal formatado.';
              break;
            case 'auth/network-request-failed':
              err.message =
                'Ocorreu um erro de rede (como tempo limite, conexão interrompida ou host inacessível).';
              break;

            default:
              err.message = 'consulte o administrador';
              break;
          }
          this.erroRecuperar.emit(err);
        }
      });
  }

  isLogado(): boolean {
    if (localStorage.getItem(this.LOCALSTORAGE_KEY)) {
      return true;
    }
    return false;
  }

  sair() {
    console.log('saindo');

    return new Promise((resolve, reject) => {
      this.afAuth.auth
        .signOut()
        .then(info => {
          resolve();
          localStorage.setItem(this.LOCALSTORAGE_KEY, 'false');
          localStorage.removeItem(this.LOCALSTORAGE_KEY);
          localStorage.removeItem(this.LOCALSTORAGE_CURRENT);
          this.usuario = {};
          localStorage.clear();
          this.depoisDeDeslogar.emit();
        })
        .catch(err => {
          console.log(err);
          reject()
        });
    });
  }

  private _recuperarInfoUsuario(user) {
    const atual = localStorage.getItem(this.LOCALSTORAGE_CURRENT);
    if (atual) {
      this.usuario = JSON.parse(atual);
      this.depoisDeLogar.emit();
    }
  }
}
