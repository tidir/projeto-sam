import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { MapProvider } from '../../providers/map/map';
import { ContatoClinicaPage } from '../contato-clinica/contato-clinica';

/**
 * Generated class for the DetalheClinicaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalhe-clinica',
  templateUrl: 'detalhe-clinica.html'
})
export class DetalheClinicaPage {
  pontoAt: any;
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  planos: any;
  categorias: any;

  constructor(
    public navCtrl: NavController,
    public maps: MapProvider,
    public platform: Platform,
    private navParams: NavParams
  ) {}

  ionViewDidLoad() {
    this.pontoAt = this.navParams.get('pa');
    this.planos = this.navParams.get('planos');
    this.categorias = this.navParams.get('categorias');
    console.log(this.pontoAt);

    this.platform.ready().then(() => {
      let mapLoaded = this.maps.init(
        this.mapElement.nativeElement,
        this.pleaseConnect.nativeElement,
        this.pontoAt.localizacao.lat,
        this.pontoAt.localizacao.long
      );
    });
  }

  goToContato() {
    this.navCtrl.push(ContatoClinicaPage, {
      pa: this.pontoAt,
      planos: this.planos,
      categorias: this.categorias
    });
  }
}
