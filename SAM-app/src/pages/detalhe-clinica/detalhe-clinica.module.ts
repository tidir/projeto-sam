import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalheClinicaPage } from './detalhe-clinica';

@NgModule({
  declarations: [
    DetalheClinicaPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalheClinicaPage),
  ],
})
export class DetalheClinicaPageModule {}
