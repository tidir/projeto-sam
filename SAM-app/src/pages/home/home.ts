import { ClinicaProvider } from './../../providers/clinica/clinica';
import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { PontoAtendimento } from '../../_models/ponto-atendimento';
import { DetalheClinicaPage } from '../detalhe-clinica/detalhe-clinica';
import * as GeoFire from 'geofire';
import { Geolocation } from '@ionic-native/geolocation';
import { LoadingAlertProvider } from '../../providers/loading-alert/loading-alert';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  filtroAtivo: boolean;
  pontoAtendimento: any[];
  pontoAtendimentoVetor: Array<any>;
  failSearch = false;
  planos = [];
  // [{
  //   id: 123,
  //   _criadoEm: '',
  //   _atualizadoEm: '',
  //   _criadoPor: '',
  //   nome: 'TESTE',
  //   descricao: 'descrição teste de card',
  //   planos: [{ nome: 'oftamologista' }],
  //   telefone: '(31)33858913',
  //   cnpj: '6979418198981981919816',
  //   email: 'tst@gmail.com',
  //   localizacao: { rua: 'asdfçlaskjdf' },
  //   dono: 'Jefim',
  //   __v: 0,
  //   dist: 10
  // },
  // {
  //   id: 123,
  //   _criadoEm: '',
  //   _atualizadoEm: '',
  //   _criadoPor: '',
  //   nome: 'TESTE',
  //   descricao: 'descrição teste de card',
  //   planos: [{ nome: 'oftamologista' }],
  //   telefone: '(31)33858913',
  //   cnpj: '6979418198981981919816',
  //   email: 'tst@gmail.com',
  //   localizacao: { rua: 'asdfçlaskjdf' },
  //   dono: 'Jefim',
  //   __v: 0,
  //   dist: 5
  // }
  // ];
  myInput: string;
  search: boolean;
  categorias: any;
  constructor(
    public navCtrl: NavController,
    private ldgAlrtProvider: LoadingAlertProvider,
    private alertCtrl: AlertController,
    private pontoAtendimentoProvider: ClinicaProvider,
    private geolocation: Geolocation
  ) {}

  ionViewDidLoad() {
    this.ldgAlrtProvider.showLoading();
    this.pontoAtendimentoProvider.recuperaPA();

    this.pontoAtendimentoProvider.aposRecuperarPA.subscribe(data => {
      this.pontoAtendimentoVetor = data.pAtend;
      this.planos = data.planos;
      this.categorias = data.categorias;
      this.geolocation.getCurrentPosition().then(position => {
        let myLat = position.coords.latitude;
        let myLng = position.coords.longitude;
        let lat;
        let lng;
        this.pontoAtendimentoVetor.forEach((pA, i) => {
          let location1 = [myLat, myLng];
          let location2 = [pA.localizacao.lat, pA.localizacao.long];
          if (pA.localizacao.lat) {
            let distance = GeoFire.distance(location1, location2);
            this.pontoAtendimentoVetor[i].distancia = Number(
              distance.toFixed(2)
            );
          }
        });
        this.pontoAtendimento = this.pontoAtendimentoVetor.sort((a, b) => {
          return a.distancia - b.distancia;
        });
        this.ldgAlrtProvider.dismissLoad();
      });
    });
  }

  // Cancela Busca ---------------------------
  onCancel() {
    this.search = false;
    this.pontoAtendimentoVetor = [];
    // this.iterateVetor(this.pontoAtendimentoVetor, this.pontoAtendimentoVetor);
    // this.meuspontoAtendimento = this.meuspontoAtendimentoVetor;
  }
  // Abre Busca ---------------------------
  searchBar() {
    this.search = !this.search;
    this.myInput = '';
    this.pontoAtendimentoVetor;
    this.onInput();
  }
  onInput() {
    console.log(this.myInput);

    if (this.myInput.length >= 3) {
      if (this.filtroAtivo === false) {
        this.pontoAtendimento = this.pontoAtendimentoVetor.filter(pAtend => {
          if (!pAtend.descricao) {
            pAtend.descricao = '';
          }
          return (
            pAtend.descricao
              .toUpperCase()
              .indexOf(this.myInput.toUpperCase()) !== -1 ||
            pAtend.nome.toUpperCase().indexOf(this.myInput.toUpperCase()) !==
              -1 ||
            pAtend.telefone
              .toUpperCase()
              .indexOf(this.myInput.toUpperCase()) !== -1 ||
            pAtend.localizacao.enderecoCompleto
              .toUpperCase()
              .indexOf(this.myInput.toUpperCase()) !== -1
          );
        });
      } else {
        this.pontoAtendimento = this.pontoAtendimento.filter(pAtend => {
          if (!pAtend.descricao) {
            pAtend.descricao = '';
          }
          return (
            pAtend.descricao
              .toUpperCase()
              .indexOf(this.myInput.toUpperCase()) !== -1 ||
            pAtend.nome.toUpperCase().indexOf(this.myInput.toUpperCase()) !==
              -1 ||
            pAtend.telefone
              .toUpperCase()
              .indexOf(this.myInput.toUpperCase()) !== -1 ||
            pAtend.localizacao.enderecoCompleto
              .toUpperCase()
              .indexOf(this.myInput.toUpperCase()) !== -1
          );
        });
      }
      if (this.pontoAtendimento.length === 0) {
        this.failSearch = true;
      } else {
        this.failSearch = false;
      }
    } else {
      this.failSearch = false;
      this.pontoAtendimento = this.pontoAtendimentoVetor;
    }
  }
  detalhesClinica(pontoAt) {
    this.navCtrl.push(DetalheClinicaPage, {
      pa: pontoAt,
      planos: this.planos,
      categorias: this.categorias
    });
  }
  filtro() {
    this.pontoAtendimento = [];
    this.pontoAtendimento = this.pontoAtendimentoVetor;
    this.failSearch = false;
    // this.filtroAtivo = false;
    this.search = false;
    this.myInput = '';
    this.onInput();

    let alert = this.alertCtrl.create();
    alert.setTitle('Filtrar por categorias');

    this.planos.forEach((element: any) => {
      alert.addInput({
        type: 'checkbox',
        label: element.nome,
        name: 'input-' + element.nome,
        id: 'input-' + element.nome,
        value: element.nome,
        checked: false
      });
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Checkbox data:', data);
        // this.pontoAtendimento = [];
        // this.pontoAtendimento = this.pontoAtendimentoVetor;
        // this.failSearch = false;
        if (data.length > 0) {
          // this.onFilter = true;
          this.pontoAtendimento = this.pontoAtendimentoVetor.filter(pAtend => {
            let ponto: any;
            if (!pAtend.planos) {
              pAtend.planos = '';
            }
            let planos: string[] = pAtend.planos;
            data.forEach(element => {
              console.log(pAtend);
              if (planos.indexOf(element) !== -1) {
                ponto = pAtend;
              }
            });
            console.log(ponto);

            return ponto;
          });
          if (this.pontoAtendimento.length === 0) {
            this.failSearch = true;
            this.filtroAtivo = false;
          } else {
            this.failSearch = false;
            this.filtroAtivo = true;
          }
        }
      }
    });

    alert.present();
  }
}
