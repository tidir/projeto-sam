import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContatoClinicaPage } from './contato-clinica';

@NgModule({
  declarations: [
    ContatoClinicaPage,
  ],
  imports: [
    IonicPageModule.forChild(ContatoClinicaPage),
  ],
})
export class ContatoClinicaPageModule {}
