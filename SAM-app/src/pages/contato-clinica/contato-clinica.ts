import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingAlertProvider } from '../../providers/loading-alert/loading-alert';

/**
 * Generated class for the ContatoClinicaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contato-clinica',
  templateUrl: 'contato-clinica.html'
})
export class ContatoClinicaPage {
  solicitacao = {
    idUsuario: '',
    idClinica: '',
    horario: '',
    especialidade: '',
    plano: '',
    observacoes: ''
  };
  time = '';
  pontoAt: any;
  planos: any;
  categorias: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private ldgAlrtProvider: LoadingAlertProvider
  ) {}
  enviarSolicitacao() {
    this.ldgAlrtProvider.showLoading();
    let obj = {
      title: 'Concluido',
      msg: 'Sua solicitação foi enviada com sucesso',
      class: '',
      cancel: '',
      confirm: 'OK'
    };

    this.ldgAlrtProvider.openDialog(obj).then(() => {
      this.navCtrl.setRoot(HomePage);
    });
    this.ldgAlrtProvider.dismissLoad();
  }

  ionViewDidLoad() {
    this.pontoAt = this.navParams.get('pa');
    this.planos = this.navParams.get('planos');
    this.categorias = this.navParams.get('categorias');
  }
}
