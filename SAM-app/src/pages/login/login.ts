import { LoadingAlertProvider } from './../../providers/loading-alert/loading-alert';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { Usuario } from '../../_models/usuario';
import { UnsubscribeAll } from '../../utils/unsubscribe-all';
import { RecuperaSenhaPage } from '../recupera-senha/recupera-senha';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  usuario = new Usuario();
  private unsubsClass = new UnsubscribeAll();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authProvider: AuthProvider,
    private ldgAlrtProvider: LoadingAlertProvider
  ) {}

  ionViewDidLoad() {
    this.authProvider.depoisDeLogar.subscribe((user: Usuario) => {
      this.ldgAlrtProvider.dismissLoad();
      this.navCtrl.setRoot(HomePage);
    });
  }
  validaInput(input) {
    return {
      'is-valid': input.valid,
      'is-invalid': input.invalid && input.touched
    };
  }
  login() {
    this.ldgAlrtProvider.showLoading();
    this.authProvider.entrar(this.usuario.email, this.usuario.senha);
  }
  pushPagesendResetEmail() {
    this.navCtrl.push(RecuperaSenhaPage);
  }
}
