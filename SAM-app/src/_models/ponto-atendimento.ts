import { AbastractFirebaseModel } from './abstract-firebase-model';
import { Localizacao } from './localizacao';
import { Plano } from './plano';

export class PontoAtendimento extends AbastractFirebaseModel {
  nome: string;
  descricao: string;
  planos: Array<Plano>;
  telefone: string;
  cnpj: string;
  email: string;
  localizacao: Localizacao;
  dono: string;

  constructor() {
    super();
    this.planos = new Array<Plano>();
    this.localizacao = new Localizacao;
  }
}
