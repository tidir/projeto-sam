export class Papel {
  nome: string;
  nivel: number;

  constructor(nome, nivel) {
    this.nome = nome;
    this.nivel = nivel;
  }
}
